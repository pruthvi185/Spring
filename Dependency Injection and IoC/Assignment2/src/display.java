import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class display {

	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		
		Player p1 = (Player) context.getBean("player1");
		 p1.showinfo();
			
		Player p2 = (Player) context.getBean("player2");
		 
		 p2.showinfo();
       Player p3 = (Player) context.getBean("player3");
		 
		 p3.showinfo();
		  Player p4 = (Player) context.getBean("player4");
			 
			 p4.showinfo();
			  Player p5 = (Player) context.getBean("player5");
				 
				 p5.showinfo();

	}

}
