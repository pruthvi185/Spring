import java.util.List;

public class Player {
	
	private String playerid;
	private String playername;
	
	private List<Country> c;
	
	
	
	public void setPlayerid(String playerid) {
		this.playerid = playerid;
	}
	public String getPlayerid() {
		return playerid;
	}
	
	public void setPlayername(String playername) {
		this.playername = playername;
	}
	public String getPlayername() {
		return playername;
	
	}
	public List<Country> getC() {
		return c;
	}
	public void setC(List<Country> c) {
		this.c = c;
	}
	public void showinfo()
	{ 
		for(Country c1 : c)
		{
		
		System.out.println("\n Player ID:" + getPlayerid() + "\n" + "Player Name:" + getPlayername() +"\n" +"Country id:" + c1.getCountryid() + "\n" + 
				"Country Name:" + c1.getCountryname() );
	}
	
	}	
	
	
	

}
