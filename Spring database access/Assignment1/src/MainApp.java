import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class MainApp {
   public static void main(String[] args) throws IOException {
      ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

      StudentJDBCTemplate studentJDBCTemplate = 
         (StudentJDBCTemplate)context.getBean("studentJDBCTemplate");
      
           

      System.out.println("------Listing All Student Records--------" );
      List<Student> students = studentJDBCTemplate.GetAllDetails();
      
      for (Student record : students) {
         System.out.print("Student ID : " + record.getStudentid() );
         System.out.print(", Student Name : " + record.getStudentname() );
         System.out.println(", Student Address : " + record.getStudentadd());
      }
         
      System.out.println("\n\n\n\n\n\n\n------------!!!!!!!!Enter the Student ID to get his record !!!!!!!!!!!!!!--------------");
      BufferedReader reader = 
              new BufferedReader(new InputStreamReader(System.in));
  
             String name = reader.readLine();
     
      System.out.println("----Listing Record with ID = " + name );
      
      Student student = studentJDBCTemplate.GetDetails( name);
      System.out.print("Student ID : " + student.getStudentid() );
      System.out.print(", Student Name : " + student.getStudentname() );
      System.out.println(", Student Address : " + student.getStudentadd());
   }
}
