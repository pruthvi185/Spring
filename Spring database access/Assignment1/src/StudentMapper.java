	
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import org.springframework.jdbc.core.RowMapper;


	   public class StudentMapper implements RowMapper<Student> {
	   public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
	      Student student = new Student();
	      student.setStudentid(rs.getString("studentid"));
	      student.setStudentname(rs.getString("studentname"));
	      student.setStudentadd(rs.getString("studentadd"));
	      
	      return student;
	   }
	}

